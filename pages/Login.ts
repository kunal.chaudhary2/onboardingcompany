// loginPage.ts
import { Page } from '@playwright/test';

export class LoginPage {
  private page: Page;
  

  constructor(page: Page) {
    this.page = page;
  }

  async navigate() {
    await this.page.goto("http://localhost:4200/welcome");
  }

  async login(username: string, password: string) {
    await this.page.click("//button[normalize-space()='Log in']");
    await this.page.locator("//input[@id='username-input']").fill(username);
    await this.page.locator("//input[@placeholder='Password']").fill(password);
    await this.page.click("//button[normalize-space()='Log in']");
    await this.page.locator("//a[normalize-space()='Management']").click();
    await this.page.locator(".dropdown-item[ng-reflect-router-link='/settings']").click();
    await this.page.locator("//a[.='Create Company']").click();
  }
}