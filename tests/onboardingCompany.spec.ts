// @ts-ignore
import { test, expect } from '@playwright/test';
import { LoginPage } from '../pages/Login';
import companyData from './testdata/country.json'; 

test.beforeEach(async ({ page }) => {
    const login = new LoginPage(page);
    await login.navigate();
    await login.login("fake.sally.super@test.com", "fake.sally.super");
    await page.getByRole('button',{ name:' Create a new company '}).click();
});

Object.entries(companyData).forEach(([country, fields], index) => {
    test(`create company with correct data set ${country}`, { tag: "@positive" }, async ({ page }) => {
        for (const field of fields) {
            switch (field.type) {
                case "text":
                    await page.getByRole('textbox', { name: `${field.placeholder}` }).fill(`${field.value}`);
                    break;
                case "dropdown":
                    await page.locator(`[formcontrolname='${field.label}']`).click();
                    await page.getByText(`${field.value}`).click(); 
                    break;
                case "abn":
                    await page.locator(`[formcontrolname='${field.label}'] input[type='text']`).fill(`${field.value}`);
                    break;
                default:
                    console.warn(`Unsupported field type: ${field.type}`);
            }
          



        }
        
        await page.getByRole('button', { name: 'Submit' }).click();
        const validationToast = await page.getByText("Company updated");
        await expect(validationToast).toBeVisible();
    });
});
